# README #


http://hegemon.ucsd.edu/~jix029/cse100/public/#/home

* username: tester
* password: secret

### What is this repository for? ###

* Quick summary

This project is intended for education use. The purpose is to let students run their codes on the server's environment before they submit for grading. Doing so, it can avoid some compile errors and other avoidable mistakes they might have.

* TO_DO

The app needs to be modulized. The whole file structure has to be refactored according to [https://github.com/johnpapa/angular-styleguide](Link URL). Change the backend using a framework for being more maintainable.