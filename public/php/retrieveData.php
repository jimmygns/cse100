<?php
include 'connectdb.php';

session_start();

$user = $_SESSION['user'];

$sql="SELECT * FROM testaccounts WHERE email='$user'";

if (!$result = $mysqli->query($sql)) {
    // Oh no! The query failed. 
    echo "Sorry, the website is experiencing problems.";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit();
}

$count=$result->num_rows;
if($count!=1){
    echo "unexpected error";
    exit();
}

$row = $result->fetch_assoc();
$json_array = json_encode($row);

echo $json_array;
exit();

?>
