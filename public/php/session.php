<?php

session_start();

if(!isset($_SESSION['user'])){
	echo "no session found";
	exit();
}

if ($_SESSION['timeout'] + 10 * 60 < time()) {
     // session timed out
	session_unset();
	session_destroy();
	echo "session timeout";
	exit();
} 
else {
	$_SESSION['timeout']=time();
    echo "loggedin";
	exit();
}


?>