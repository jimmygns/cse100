<?php
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


$servername = "localhost";
$dbusername = "cse100";
$dbpassword = "fall2015";
$dbname = "cse100f15";

$mysqli = new mysqli($servername, $dbusername, $dbpassword,$dbname);

$email = $mysqli->real_escape_string($request->email);
$password = $mysqli->real_escape_string($request->password);
$studentId = $mysqli->real_escape_string($request->studentId);
$firstName = $mysqli->real_escape_string($request->firstName);
$lastName = $mysqli->real_escape_string($request->lastName);

if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    exit();
}

$options = [
    'cost' => 12,
];
$hash = password_hash($password, PASSWORD_BCRYPT, $options);

$sql = "INSERT INTO testaccounts (email, password, studentId, active, firstName, lastName) 
VALUES ('$email','$hash','$studentId','y','$firstName','$lastName')";

if (!$result = $mysqli->query($sql)) {
    // Oh no! The query failed. 
    echo "Sorry, the website is experiencing problems.";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit();
}

echo "success";
exit();

?>
