<?php

include 'connectdb.php';

session_start();

$user = $_SESSION['user'];

$targetdir = "testfolder/";
$targetfolder = $targetdir . $user . ".zip";

try {

    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
if (!isset($_FILES['file']['error']) ||is_array($_FILES['file']['error'])) {
  throw new RuntimeException('Invalid parameters.');
}

    // Check $_FILES['upfile']['error'] value.
switch ($_FILES['file']['error']) {
  case UPLOAD_ERR_OK:
  break;
  case UPLOAD_ERR_NO_FILE:
  throw new RuntimeException('No file sent.');
  case UPLOAD_ERR_INI_SIZE:
  case UPLOAD_ERR_FORM_SIZE:
  throw new RuntimeException('Exceeded filesize limit.');
  default:
  throw new RuntimeException('Unknown errors.');
}

    // You should also check filesize here. 
if ($_FILES['file']['size'] > 6000000) {
  throw new RuntimeException('Exceeded filesize limit.');
}

if(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION)!="zip"){
  throw new RuntimeException('Invalid file format.');
}


    // You should name it uniquely.
    // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
    // On this example, obtain safe unique name from its binary data.
if (!move_uploaded_file($_FILES['file']['tmp_name'], $targetfolder)) {
  throw new RuntimeException('Failed to move uploaded file.');
}

$sql="UPDATE testaccounts SET readyfortest=1 WHERE email='$user'";
if (!$mysqli->query($sql)) {
    // Oh no! The query failed. 
    echo "Sorry, the website is experiencing problems.";
    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit();
}
echo 'success';
exit();

} 
catch (RuntimeException $e) {

  echo $e->getMessage();
  exit();

}

?>

