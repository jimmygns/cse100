<?php

$handle = fopen("configuration.txt", "r");
if ($handle) {
	$array=array();
    while (($buffer = fgets($handle)) !== false) {
        array_push($array, trim($buffer));
    }
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    $json_array = json_encode($array);
    echo $json_array;
    fclose($handle);
    exit();
}

?>