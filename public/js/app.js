var theApp = angular.module('theApp', ['ngRoute']);

theApp.config(['$routeProvider', 
	function ($routeProvider) {


		var onlyLoggedIn = function($http,$q,$location){
			var deferred = $q.defer();
			var request = $http({
				method: "get",
				url: "php/session.php"
			});
			request.success(function(response){
				if(response=='loggedin'){
					deferred.resolve();
				}
				else{
					deferred.reject();
					$location.url('/login');

				}
			});
			return deferred.promise;

		};

		var alreadyLoggedIn = function($http,$q,$location){
			var deferred = $q.defer();
			var request = $http({
				method: "get",
				url: "php/session.php"
			});
			request.success(function(response){
				if(response=='loggedin'){
					deferred.reject();
					$location.url('/profile');
				}
				else{
					deferred.resolve();
					

				}
			});
			return deferred.promise;

		};



		$routeProvider

            // route for the signup page
            .when('/signup', {
            	templateUrl : 'html/signup.html',
            	controller  : 'signupController',
            	resolve:{alreadyLoggedIn:alreadyLoggedIn}
            })

            // route for the home page
            .when('/login', {
            	templateUrl : 'html/login.html',
            	controller  : 'loginController',
            	resolve:{alreadyLoggedIn:alreadyLoggedIn}
            })
            .when('/home', {
            	templateUrl : 'html/home.html'
            })
            .when('/profile', {
            	templateUrl : 'html/profile.html',
            	controller  : 'profileController',
            	resolve:{loggedIn:onlyLoggedIn}
            })
            .when('/setting', {
            	templateUrl :'html/setting.html',
            	resolve:{loggedIn:onlyLoggedIn}
            })
            .otherwise({
            	redirectTo: '/home'
            });


            
        }]);


/***************************/
/*
nav bar controller
*/
theApp.controller("navController", function($scope,$http,$location,$route){
	var config = $http({
		method: "get",
		url: "php/getConfig.php"
	});

	config.success(function(response){
		var line = response[1].split(":");
		var hw_line = line[1].split(",");
		var i;
		var hws=[];
		for(i=0;i<hw_line.length;i++){
			var name = hw_line[i];
			hws.push(
			{
				name: name,
			});
		}
		$scope.hws=hws;


	});



	$scope.logout = function(){
		var request = $http({
			method:"get",
			url: "php/logout.php"

		});
		$scope.isLoggedin=false;
		
		$location.path('/login');
	};


	$scope.$on('userLoggedin', function(event,response){
		$scope.isLoggedin=true;
		var firstName = response.firstName;
		var lastName = response.lastName;
		var fullName = firstName+" "+lastName;
		$scope.fullName=fullName;

	});
	$scope.$on('userLoggedout', function(event){
		$scope.isLoggedin=false;
	});
});


/***************************/

/*
signup controller

*/

theApp.controller("signupController" , function($scope,$http,$location){
	$scope.signup = function(){
		var email = $scope.email;
		var password = $scope.password;
		var studentId = $scope.studentId;
		var firstName = $scope.firstName;
		var lastName = $scope.lastName;
		//alert(email+password+studentId);
		$http({
			method: "post",
			url: "php/signup.php",
			data: {
				email: email,
				password: password,
				studentId: studentId,
				firstName: firstName,
				lastName: lastName
			},
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(response){
			if(response=="success"){
				alert("Success!");
				$location.path('/login');
			}
			else{
				alert(response);
			}
			
		});
	}

	
});

/***************************/

theApp.controller("loginController" , function($scope,$http,$location,$route){

	var request = $http({
		method: "get",
		url: "php/session.php"
	});

	request.success(function(response){
		if(response=='loggedin'){
			$location.path('/profile');
		}
		else{
			$scope.$emit('userLoggedout');
		}
	});

	$scope.login = function(){
		var email = $scope.email;
		var password = $scope.password;
		
		var request = $http({
			method: "post",
			url: "php/login.php",
			data: {
				email: email,
				password: password,
			},
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		});
		request.success(function(response){
			if(response=='success'){
				$location.path('/profile');
			}
			else{
				alert(response);
			}
		});
	}
});

/***************************/

theApp.controller("profileController" , function($scope,$http,$location,$window){
	var request = $http({
		method: "get",
		url: "php/session.php"
	});

	request.success(function(response){
		if(response=='loggedin'){
			var data = $http({
				method:"get",
				url: "php/retrieveData.php"
			});

			data.success(function(response1){

				$scope.$emit('userLoggedin',response1);
				var config = $http({
					method: "get",
					url: "php/getConfig.php"
				});
				config.success(function(response2){
					//getting the available grades for the current logged in user
					var line = response2[0].split(":");
					var avail_grades = line[1].split(",");
					var i;
					var grades=[];
					for(i =0;i<avail_grades.length;i++){
						var name = avail_grades[i];
						var score = response1[name];
						grades.push(
						{
							name: name,
							score: score,

						});
					}
					$scope.grades=grades;
					//rendering the hw submissions
					var line2 = response2[2].split(":");
					var currentHW = line2[1];
					$scope.currentHW=currentHW;

					$scope.clicked=false;
					var file = $http({
						method: "get",
						url: "php/getTestResult.php"
					});
					file.success(function(fileContent){
						$scope.testResultContent=fileContent;
					});


				});

			});

			
		}
		else{
			$scope.$emit('userLoggedout');
			$location.path('/login');
		}

	});

	$scope.toggle = function(){

		if($scope.clicked){
			$scope.clicked=false;
		}
		else{
			$scope.clicked=true;
		}

	};

	$scope.fileUpload = function(event){

		var request = $http({
			method: "get",
			url: "php/session.php"
		});

		request.success(function(response){
			if(response=='loggedin'){
				var form=document.getElementById('fileUpload');
				var data = new FormData(form);
				data.append('file',file);
				var id = event.target.getAttribute('id');

				if(id=="forTest"){
					var request=$http({
						method: "post",
						url: "php/fileUploadTest.php",
						data: data,
						headers:{'Content-Type': undefined}
					});
					request.success(function(response){
						if(response=="success"){
							alert("successfully uploaded for test!")
							document.getElementById("fileUpload").reset();
							$scope.fileName="";
						}
						else{
							alert(response);
						}
					});
				}
				else{
					var request=$http({
						method: "post",
						url: "php/fileUploadGrade.php",
						data: data,
						headers:{'Content-Type': undefined}
					});
					request.success(function(response){
						if(response=="success"){
							alert("successfully uploaded for grade!")
							document.getElementById("fileUpload").reset();
							$scope.fileName="";
						}
						else{
							alert(response);
						}
					});

				}
			}
			else{
				$scope.$emit('userLoggedout');
				$location.path('/login');
			}
		});

}

$scope.fileSelected = function(element){
	var f = element.files[0];
	$scope.fileName=f.name;
	$scope.$apply();
}

});



theApp.controller('settingController', ['$scope','$location','$http', function($scope,$location,$http) {
	var request = $http({
		method: "get",
		url: "php/session.php"
	});

	request.success(function(response){
		if(response=='loggedin'){
			var setting = $http({
				method: "get",
				url: "php/retrieveData.php"
			});
			setting.success(function(response1){
				console.log(response1["emailsetting"]);
				if(response1["emailsetting"]==1){
					$scope.checkboxValue=true;
				}
				else if(response1["emailsetting"]==0){
					$scope.checkboxValue=false;
				}
				else{
					alert("unexpected error");
				}
			});
		}
		else{
			$scope.$emit('userLoggedout');
			$location.path('/login');
		}
	});


	$scope.save = function(){
		var request = $http({
			method: "get",
			url: "php/session.php"
		});

		request.success(function(response){
			if(response=='loggedin'){
				var emailsetting = $scope.checkboxValue;
				var saveSetting = $http({
					method: "post",
					url: "php/saveSetting.php",
					data: {
						emailsetting: emailsetting
					}
				});

				saveSetting.success(function(response2){
					if(response2=="success"){
						alert("your setting has been updated");
					}
					else{
						alert("unexpected error");
					}
				});
			}
			else{
				$scope.$emit('userLoggedout');
				$location.path('/login');
				
			}
		});

		
	};
}]);


